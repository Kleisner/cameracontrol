import React, { Component } from "react";
import {
  SafeAreaView,
  View,
  StyleSheet,
  ActivityIndicator
} from "react-native";
import axios from "axios";

import Tabs from "./src/Tabs";
import Camera from "./src/Camera";

import { CameraItem } from "./types/types";
import CameraList from "./src/CameraList";
import Control from "./src/Control";

interface State {
  selectedTab: string;
  selectedCameraId?: string;
  cameras?: CameraItem[];
}

class App extends Component<{}, State> {
  private cameraRef: Camera | null = null;

  public state: State = {
    selectedTab: "Cameras"
  };

  private renderCameraList = () => {
    return (
      <CameraList
        selectedCameraId={this.state.selectedCameraId}
        onSelectCamera={selectedCameraId => {
          this.setState({
            selectedCameraId
          });

          if (this.cameraRef) {
            this.cameraRef.reset();
          }
        }}
        cameras={this.state.cameras || []}
      />
    );
  };

  private renderControl = () => {
    return <Control move={this.cameraRef && this.cameraRef.move} />;
  };

  public async componentDidMount() {
    const camerasResponse = await axios.get(
      "http://runningios.com/screamingbox/cameras.json"
    );

    const cameras = camerasResponse.data;

    this.setState({
      cameras,
      selectedCameraId:
        Array.isArray(cameras) && cameras.length > 0 && cameras[0].id
    });
  }

  private renderLoader() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator />
      </View>
    );
  }

  private renderContent = () => {
    const { selectedTab, selectedCameraId, cameras } = this.state;

    const tabs = {
      Cameras: this.renderCameraList,
      Control: this.renderControl
    };

    const camera = cameras && cameras.find(c => c.id === selectedCameraId);

    return (
      <>
        <View style={styles.panel}>
          {camera ? (
            <Camera
              ref={ref => (this.cameraRef = ref)}
              cameraUrl={camera.source}
            />
          ) : (
            this.renderLoader()
          )}
        </View>
        <View style={styles.panel}>
          <Tabs
            tabs={tabs}
            selectedTab={selectedTab}
            onTabSelectionChange={selectedTab => this.setState({ selectedTab })}
          />
        </View>
      </>
    );
  };

  public render() {
    return (
      <SafeAreaView style={styles.container}>
        {this.state.cameras ? this.renderContent() : this.renderLoader()}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  panel: {
    flex: 0.5,
    backgroundColor: "white"
  }
});

export default App;
