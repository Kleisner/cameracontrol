import React, { ComponentType, ReactNode } from "react";
import { ButtonGroup } from "react-native-elements";

interface Props {
  selectedTab: string;
  tabs: {
    [key: string]: () => ReactNode;
  };
  onTabSelectionChange: (selectedTab: string) => void;
}

export default ({ selectedTab, tabs, onTabSelectionChange }: Props) => {
  const renderTabContent = tabs[selectedTab];

  const tabsKeys = Object.keys(tabs);

  return (
    <>
      <ButtonGroup
        buttons={Object.keys(tabs)}
        selectedIndex={tabsKeys.indexOf(selectedTab)}
        onPress={index => onTabSelectionChange(tabsKeys[index])}
      />
      {renderTabContent()}
    </>
  );
};
