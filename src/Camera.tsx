import React, { Component } from "react";
import { Animated, View, StyleSheet, Image } from "react-native";

interface Props {
  cameraUrl: string;
}

interface State {
  imageWidth: number;
  imageHeight: number;
}

const defaultDimensions = { imageWidth: 100, imageHeight: 100 };

export default class Camera extends Component<Props, State> {
  public state: State = { ...defaultDimensions };

  private x = 0;
  private y = 0;
  private xAnim = new Animated.Value(0);
  private yAnim = new Animated.Value(0);
  private layoutWidth = 0;
  private layoutHeight = 0;

  public move = (dx: number, dy: number) => {
    const newX = this.x - dx;
    const newY = this.y - dy;

    const { imageWidth, imageHeight } = this.state;

    if (newX < 0 && newX > (this.layoutWidth -imageWidth)) {
      this.x = newX;
      this.xAnim.setValue(this.x);
    }

    if (newY < 0 && newY > (this.layoutHeight - imageHeight)) {
      this.y = newY;
      this.yAnim.setValue(this.y);
    }
  };

  public reset = () => {
      this.x = 0;
      this.y = 0;
      this.xAnim.setValue(this.x);
      this.yAnim.setValue(this.y);
  }

  public componentDidMount() {
    Image.getSize(
      this.props.cameraUrl,
      (imageWidth, imageHeight) => {
        this.setState({ imageWidth, imageHeight });
      },
      () => {
        this.setState(defaultDimensions);
      }
    );
  }

  public render() {
    const { imageWidth, imageHeight } = this.state;

    return (
      <View
        style={styles.container}
        onLayout={({nativeEvent: { layout }}) => {
          this.layoutWidth = layout.width
          this.layoutHeight = layout.height
        }}
      >
        <Animated.Image
          style={{
            width: imageWidth,
            height: imageHeight,
            transform: [{ translateX: this.xAnim }, { translateY: this.yAnim }]
          }}
          source={{ uri: this.props.cameraUrl }}
        ></Animated.Image>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
