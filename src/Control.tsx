import React, { Component } from "react";
import {
  View,
  StyleSheet,
  PanResponder,
  PanResponderInstance
} from "react-native";
import { Text } from "react-native-elements";

interface Props {
  move: ((dx: number, dy: number) => void) | null;
}

export default class Control extends Component<Props> {
  private panResponder: PanResponderInstance;

  public constructor(props: Props) {
    super(props);
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => {
        if (props.move) {
          props.move(gestureState.dx, gestureState.dy);
        }
      }
    });
  }

  public render() {
    return (
      <View style={styles.container}>
          <Text>Click in circle & Drag</Text>
        <View style={styles.circleControl} {...this.panResponder.panHandlers} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  circleControl: {
    width: 150,
    height: 150,
    borderWidth: 1,
    borderColor: "lightgrey",
    borderRadius: 75
  }
});
