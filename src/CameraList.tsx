import React from "react";
import { Text } from "react-native-elements";

import { CameraItem } from "../types/types";
import { ScrollView, TouchableOpacity, StyleSheet } from "react-native";

interface Props {
  cameras: CameraItem[];
  onSelectCamera: (selectedCameraId?: string) => void;
  selectedCameraId?: string;
}

export default ({ cameras, onSelectCamera, selectedCameraId }: Props) => (
  <ScrollView>
    {cameras.map((camera, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => onSelectCamera(camera.id)}
        style={[styles.item, camera.id === selectedCameraId && styles.selected]}
      >
        <Text>{camera.name}</Text>
      </TouchableOpacity>
    ))}
  </ScrollView>
);

const styles = StyleSheet.create({
  item: {
    borderBottomWidth: 1,
    borderBottomColor: "lightgrey",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  selected: {
    backgroundColor: "aliceblue"
  }
});
