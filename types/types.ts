export interface CameraItem {
    id: string
    name: string
    source: string
}